package appisode.fascinatingnumbers;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class NumberAdapter extends BaseAdapter {
    LayoutInflater mInflater;
    int mAvailable;
    int mTimeLimit;
    OnNumberAdapterEventListener mOnNumberAdapterEventListener;
    public NumberAdapter(LayoutInflater inflater, int timeLimit) {
        mInflater = inflater;
        mTimeLimit = timeLimit;
        initialize();
        provideMore();
    }

    public View inflate(int resource) {
        return mInflater.inflate(resource, null);
    }

    public void addAvailable(int count) {
        mAvailable += count;
        notifyDataSetChanged();
    }

    public int getCount() {
        return mAvailable;
    }
    public int getTimeLimit() {
        return mTimeLimit;
    }
    public Object getItem(int i) {
        return null;
    }
    public long getItemId(int i) {
        return 0;
    }
    public abstract View getView(int i, View view, ViewGroup viewGroup);
    public abstract void provideMore();
    public abstract void initialize();
    public void onLimit() {
        if(mOnNumberAdapterEventListener != null)
            mOnNumberAdapterEventListener.onLimit();
    }
    public void onLoadingStarted() {
        if(mOnNumberAdapterEventListener != null)
            mOnNumberAdapterEventListener.onLoadingStarted();
    }
    public void onLoadingStopped() {
        if(mOnNumberAdapterEventListener != null)
            mOnNumberAdapterEventListener.onLoadingStopped();
    }
    public void setOnNumberAdapterEventListener(OnNumberAdapterEventListener listener) {
        mOnNumberAdapterEventListener = listener;
    }
    public interface OnNumberAdapterEventListener {
        public void onLimit();
        public void onLoadingStarted();
        public void onLoadingStopped();
    }
}