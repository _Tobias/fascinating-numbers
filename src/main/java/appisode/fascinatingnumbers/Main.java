package appisode.fascinatingnumbers;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import appisode.fascinatingnumbers.sequences.Catalan;
import appisode.fascinatingnumbers.sequences.Fibonacci;
import appisode.fascinatingnumbers.sequences.LazyCaterer;
import appisode.fascinatingnumbers.sequences.Lucas;
import appisode.fascinatingnumbers.sequences.Pell;
import appisode.fascinatingnumbers.sequences.Primes;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;


public class Main extends ActionBarActivity implements DialogInterface.OnClickListener {
    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    SharedPreferences mPrefs;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPrefs = PreferenceManager.getDefaultSharedPreferences(Main.this);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), mPrefs.getInt(PREF_LIMIT_ENUM, 0));
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }

    static String PREF_LIMIT_ENUM = "limit";
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItemCompat.setShowAsAction(menu.add(R.string.time_limit).setIcon(R.drawable.ic_action_time).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem menuItem) {
                new AlertDialog.Builder(Main.this)
                    .setTitle(R.string.time_limit)
                    .setSingleChoiceItems(R.array.time_limits, mPrefs.getInt(PREF_LIMIT_ENUM, 0), Main.this)
                    .show();
                return true;
            }
        }), MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    static int[] TITLES = new int[] {R.string.fibonacci, R.string.lucas, R.string.primes, R.string.catalan, R.string.lazy_caterer, R.string.pell};
    static int[] DESCRIPTIONS = new int[] {R.string.fibonacci_description, R.string.lucas_description, R.string.primes_description, R.string.catalan_description, R.string.lazy_caterers_description, R.string.pell_description};
    static int[] IMAGES = new int[] {0, 0, 0, 0, 0, 0};

    public void onClick(DialogInterface dialogInterface, int i) {
        mPrefs.edit().putInt(PREF_LIMIT_ENUM, i).commit();
        Toast.makeText(this, R.string.restart_app, Toast.LENGTH_LONG).show();
        dialogInterface.dismiss();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        int mTimeLimit;
        public SectionsPagerAdapter(FragmentManager fm, int limit) {
            super(fm);
            switch(limit) {
                case 0: mTimeLimit = 400; break;
                case 1: mTimeLimit = 1000; break;
                case 2: mTimeLimit = 2500; break;
                case 3: mTimeLimit = 5000; break;
                case 4: mTimeLimit = 10000; break;
                case 5: mTimeLimit = 0; break;
            }
        }

        public Fragment getItem(int position) {
            return NumberFragment.newInstance(position, mTimeLimit);
        }

        public int getCount() {
            return TITLES.length;
        }

        public CharSequence getPageTitle(int position) {
            return getText(TITLES[position]);
        }
    }

    public static class NumberFragment extends Fragment implements AbsListView.OnScrollListener, NumberAdapter.OnNumberAdapterEventListener, Runnable {
        private static final String ARG_TYPE = "type";
        private static final String ARG_LIMIT = "limit";

        public static NumberFragment newInstance(int type, int timeLimit) {
            NumberFragment fragment = new NumberFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_TYPE, type);
            args.putInt(ARG_LIMIT, timeLimit);
            fragment.setArguments(args);
            return fragment;
        }

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }

        NumberAdapter mAdapter;
        ListView mListView;
        SmoothProgressBar mProgressBar;
        int firstVisibleItem;
        TextView mBottomView;
        int mTimeLimit;
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            mTimeLimit = getArguments().getInt(ARG_LIMIT);
            View root = inflater.inflate(R.layout.fragment_number, null);
            mListView = (ListView)root.findViewById(R.id.list);
            mProgressBar = (SmoothProgressBar)root.findViewById(R.id.progress_bar);
            int type = getArguments().getInt(ARG_TYPE);
            if(mAdapter == null) {
                switch(type) {
                    case 0: mAdapter = new Fibonacci(inflater, mTimeLimit); break;
                    case 1: mAdapter = new Lucas(inflater, mTimeLimit); break;
                    case 2: mAdapter = new Primes(inflater, mTimeLimit); break;
                    case 3: mAdapter = new Catalan(inflater, mTimeLimit); break;
                    case 4: mAdapter = new LazyCaterer(inflater, mTimeLimit); break;
                    case 5: mAdapter = new Pell(inflater, mTimeLimit); break;
                }

                mAdapter.setOnNumberAdapterEventListener(this);
            }
            if(DESCRIPTIONS[type] != 0) {
                TextView headerView = (TextView)inflater.inflate(R.layout.listview_text, null);
                headerView.setVisibility(View.VISIBLE);
                headerView.setText(DESCRIPTIONS[type]);
                mListView.addHeaderView(headerView);
            }
            if(IMAGES[type] != 0) {
                ImageView headerImage = (ImageView)inflater.inflate(R.layout.listview_image, null);
                headerImage.setImageResource(IMAGES[type]);
                mListView.addHeaderView(headerImage);
            }
            mBottomView = (TextView)inflater.inflate(R.layout.listview_text, null);
            mBottomView.setText(R.string.time_limit_reached);
            mListView.addFooterView(mBottomView);
            mListView.setAdapter(mAdapter);
            mListView.post(this);
            return root;
        }

        public void onScrollStateChanged(AbsListView absListView, int i) {}

        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if(totalItemCount != 0)
                this.firstVisibleItem = firstVisibleItem;
            this.firstVisibleItem = firstVisibleItem;
            if (++firstVisibleItem + visibleItemCount > totalItemCount && totalItemCount != 0 && visibleItemCount != totalItemCount)
                mAdapter.provideMore();
        }

        public void onLimit() {
            mBottomView.setVisibility(View.VISIBLE);
        }

        public void onLoadingStarted() {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        public void onLoadingStopped() {
            mProgressBar.setVisibility(View.GONE);
        }

        public void run() {
            mListView.setSelection(firstVisibleItem);
            mListView.setOnScrollListener(this);
        }
    }

}
