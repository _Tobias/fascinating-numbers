package appisode.fascinatingnumbers;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public abstract class NumberTextAdapter extends NumberAdapter {
    public NumberTextAdapter(LayoutInflater inflater, int timeLimit) {
        super(inflater, timeLimit);
    }

    public abstract CharSequence getText(int position);

    public final View getView(int i, View view, ViewGroup viewGroup) {
        TextView t = (TextView)(view != null ? view : inflate(android.R.layout.simple_list_item_1));
        t.setText(getText(i));
        return t;
    }
}
