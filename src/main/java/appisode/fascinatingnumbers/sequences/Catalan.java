package appisode.fascinatingnumbers.sequences;

import android.os.AsyncTask;
import android.text.Html;
import android.view.LayoutInflater;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import appisode.fascinatingnumbers.NumberTextAdapter;

public class Catalan extends NumberTextAdapter {
    public Catalan(LayoutInflater inflater, int timeLimit) {
        super(inflater, timeLimit);
    }

    public CharSequence getText(int i) {
        return Html.fromHtml("<i>C<sub>"+i+"</sub></i> = "+mNumbers.get(i));
    }

    List<BigInteger> mNumbers;
    int round;
    boolean limit;
    ProvideMore calculator;
    public void provideMore() {
        if(limit || (calculator != null && calculator.getStatus() != AsyncTask.Status.FINISHED))
            return;

        calculator = new ProvideMore();
        calculator.execute();
    }

    private class ProvideMore extends AsyncTask<Void, Void, Void> {
        long startTime;
        protected void onPreExecute() {
            startTime = System.currentTimeMillis();
            onLoadingStarted();
        }

        protected Void doInBackground(Void... voids) {
            for(int n = getCount(); n < getCount()+round; n++) {
                BigInteger num = BigInteger.ONE,
                        denom1 = num,
                        denom2 = num,
                        factorial = num,
                        factor = num;
                int k,
                        numStop = n << 1,
                        denomStop = n;

                if(n < 2) {
                    mNumbers.add(factorial);
                    continue;
                }
                for (k = 1; k <= numStop; k++) {
                    factorial = factorial.multiply(factor);
                    if(k == numStop) {
                        num = factorial;
                    }
                    else if(k == denomStop) {
                        denom1 = factorial;
                        k++;
                        factor = factor.add(BigInteger.ONE);
                        denom2 = factorial = factorial.multiply(factor);
                    }
                    factor = factor.add(BigInteger.ONE);
                }
                mNumbers.add(num.divide(denom1).divide(denom2));
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            addAvailable(round);
            if(getTimeLimit() > 0 && System.currentTimeMillis()-startTime > getTimeLimit()) {
                limit = true;
                onLimit();
            }
            onLoadingStopped();
        }
    }

    // Initialize variables that are used by a method in the constructor
    public void initialize() {
        mNumbers = new ArrayList<BigInteger>();
        round = 25;
    }
}
