package appisode.fascinatingnumbers.sequences;

import android.os.AsyncTask;
import android.text.Html;
import android.view.LayoutInflater;

import java.util.ArrayList;
import java.util.List;

import appisode.fascinatingnumbers.NumberTextAdapter;

public class Primes extends NumberTextAdapter {
    public Primes(LayoutInflater inflater, int timeLimit) {
        super(inflater, timeLimit);
    }

    public CharSequence getText(int i) {
        return Html.fromHtml("<i>P<sub>"+i+"</sub></i> = "+mNumbers.get(i));
    }

    List<Long> mNumbers;
    int round;
    long lastPrime;
    boolean limit;
    ProvideMore calculator;
    public void provideMore() {
        if(limit || (calculator != null && calculator.getStatus() != AsyncTask.Status.FINISHED))
            return;

        calculator = new ProvideMore();
        calculator.execute();
    }

    private class ProvideMore extends AsyncTask<Void, Void, Void> {
        long startTime;
        protected void onPreExecute() {
            startTime = System.currentTimeMillis();
            onLoadingStarted();
        }

        protected Void doInBackground(Void... voids) {
            int found = 0;
            while(found < round) {
                lastPrime++;
                if(isPrime(lastPrime)) {
                    found++;
                    mNumbers.add(lastPrime);
                }
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            addAvailable(round);
            if(getTimeLimit() > 0 && System.currentTimeMillis()-startTime > getTimeLimit()) {
                limit = true;
                onLimit();
            }
            onLoadingStopped();
        }
    }

    // Initialize variables that are used by a method in the constructor
    public void initialize() {
        mNumbers = new ArrayList<Long>();
        round = 50;
        lastPrime = 1;
    }

    private static boolean isPrime(long number){
        for(int i = 2; i < number; i++){
            if(number%i == 0)
                return false;
        }
        return true;
    }
}
