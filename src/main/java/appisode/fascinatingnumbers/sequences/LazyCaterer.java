package appisode.fascinatingnumbers.sequences;

import android.os.AsyncTask;
import android.text.Html;
import android.view.LayoutInflater;

import java.util.ArrayList;
import java.util.List;

import appisode.fascinatingnumbers.NumberTextAdapter;

public class LazyCaterer extends NumberTextAdapter {
    public LazyCaterer(LayoutInflater inflater, int timeLimit) {
        super(inflater, timeLimit);
    }

    public CharSequence getText(int i) {
        return Html.fromHtml("<i>f("+i+")</i> = "+mNumbers.get(i));
    }

    List<Long> mNumbers;
    int round;
    boolean limit;
    ProvideMore calculator;
    public void provideMore() {
        if(limit || (calculator != null && calculator.getStatus() != AsyncTask.Status.FINISHED))
            return;

        calculator = new ProvideMore();
        calculator.execute();
    }

    private class ProvideMore extends AsyncTask<Void, Void, Void> {
        long startTime;
        protected void onPreExecute() {
            startTime = System.currentTimeMillis();
            onLoadingStarted();
        }

        protected Void doInBackground(Void... voids) {
            for(long n = getCount(); n < getCount()+round; n++)
                mNumbers.add((n*n+n+2)/2);
            return null;
        }

        protected void onPostExecute(Void result) {
            addAvailable(round);
            if(getTimeLimit() > 0 && System.currentTimeMillis()-startTime > getTimeLimit()) {
                limit = true;
                onLimit();
            }
            onLoadingStopped();
        }
    }

    // Initialize variables that are used by a method in the constructor
    public void initialize() {
        mNumbers = new ArrayList<Long>();
        round = 50;
    }
}
