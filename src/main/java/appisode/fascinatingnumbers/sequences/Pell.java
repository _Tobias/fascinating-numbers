package appisode.fascinatingnumbers.sequences;

import android.os.AsyncTask;
import android.text.Html;
import android.view.LayoutInflater;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import appisode.fascinatingnumbers.NumberTextAdapter;

public class Pell extends NumberTextAdapter {
    public Pell(LayoutInflater inflater, int timeLimit) {
        super(inflater, timeLimit);
    }

    public CharSequence getText(int i) {
        return Html.fromHtml("<i>P<sub>"+i+"</sub></i> = "+mNumbers.get(i));
    }

    List<BigInteger> mNumbers;
    int round;
    boolean limit;
    BigInteger a;
    BigInteger b;
    ProvideMore calculator;
    public void provideMore() {
        if(limit || (calculator != null && calculator.getStatus() != AsyncTask.Status.FINISHED))
            return;

        calculator = new ProvideMore();
        calculator.execute();
    }

    static BigInteger MULTIPLY = new BigInteger("2");

    private class ProvideMore extends AsyncTask<Void, Void, Void> {
        long startTime;
        protected void onPreExecute() {
            startTime = System.currentTimeMillis();
            onLoadingStarted();
        }

        protected Void doInBackground(Void... voids) {
            for(int n = getCount(); n < getCount()+round; n++) {
                if(n == 0 || n == 1)
                    mNumbers.add(n == 0 ? a : b);
                else {
                    BigInteger sum = b.multiply(MULTIPLY).add(a);
                    a = b;
                    b = sum;
                    mNumbers.add(sum);
                }
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            addAvailable(round);
            if(getTimeLimit() > 0 && System.currentTimeMillis()-startTime > getTimeLimit()) {
                limit = true;
                onLimit();
            }
            onLoadingStopped();
        }
    }

    // Initialize variables that are used by a method in the constructor
    public void initialize() {
        mNumbers = new ArrayList<BigInteger>();
        round = 50;
        a = new BigInteger("0");
        b = new BigInteger("1");
    }
}
