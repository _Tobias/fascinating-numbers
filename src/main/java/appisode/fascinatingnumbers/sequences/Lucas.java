package appisode.fascinatingnumbers.sequences;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import appisode.fascinatingnumbers.NumberAdapter;

public class Lucas extends NumberAdapter {
    public Lucas(LayoutInflater inflater, int timeLimit) {
        super(inflater, timeLimit);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView t = (TextView)(view != null ? view : inflate(android.R.layout.simple_list_item_1));
        t.setText(Html.fromHtml("<i>L<sub>"+i+"</sub></i> = "+mNumbers.get(i)));
        return t;
    }

    List<BigInteger> mNumbers;
    BigInteger a;
    BigInteger b;
    int round;
    public void provideMore() {
        boolean firstRound = getCount() == 0;
        for(int i = 0; i < round; i++) {
            if((i == 0 || i == 1) && firstRound)
                mNumbers.add(i == 0 ? a : b);
            else {
                BigInteger sum = a.add(b);
                mNumbers.add(sum);
                a = b;
                b = sum;
            }
        }
        addAvailable(round);
    }

    // Initialize variables that are used by a method in the constructor
    public void initialize() {
        mNumbers = new ArrayList<BigInteger>();
        a = new BigInteger("2");
        b = new BigInteger("1");
        round = 50;
    }
}
