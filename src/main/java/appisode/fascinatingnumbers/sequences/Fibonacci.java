package appisode.fascinatingnumbers.sequences;

import android.text.Html;
import android.view.LayoutInflater;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import appisode.fascinatingnumbers.NumberTextAdapter;

public class Fibonacci extends NumberTextAdapter {
    public Fibonacci(LayoutInflater inflater, int timeLimit) {
        super(inflater, timeLimit);
    }

    public CharSequence getText(int i) {
        return Html.fromHtml("<i>F<sub>"+i+"</sub></i> = "+mNumbers.get(i));
    }

    List<BigInteger> mNumbers;
    BigInteger a;
    BigInteger b;
    int round;
    public void provideMore() {
        boolean firstRound = getCount() == 0;
        for(int i = 0; i < round; i++) {
            if((i == 0 || i == 1) && firstRound)
                mNumbers.add(i == 0 ? a : b); // a and b are initialized as 0 and 1
            else {
                BigInteger sum = a.add(b);
                mNumbers.add(sum);
                a = b;
                b = sum;
            }
        }
        addAvailable(round);
    }

    // Initialize variables that are used by a method in the constructor
    public void initialize() {
        mNumbers = new ArrayList<BigInteger>();
        a = new BigInteger("0");
        b = new BigInteger("1");
        round = 50;
    }
}
