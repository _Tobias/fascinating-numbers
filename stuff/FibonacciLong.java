package appisode.fascinatingnumbers.appisode.fascinatingnumbers.sequenceas;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import appisode.fascinatingnumbers.NumberAdapter;

public class FibonacciLong extends NumberAdapter {
    public FibonacciLong(LayoutInflater inflater) {
        super(inflater);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView t = (TextView)(view != null ? view : inflate(android.R.layout.simple_list_item_1));
        t.setText(Html.fromHtml("<i>F<sub>"+i+"</sub></i> = "+mNumbers.get(i)));
        return t;
    }

    List<Long> mNumbers;
    long a;
    long b;
    int round;
    boolean limit;
    public void provideMore() {
        if(limit)
            return;
        int added = round;
        boolean firstRound = getCount() == 0;
        for(int i = 0; i < round; i++) {
            if((i == 0 || i == 1) && firstRound)
                mNumbers.add((long)i);
            else {
                long sum = a+b;
                if(((Long)sum).compareTo((long)0) < 0) {
                    // finished
                    limit = true;
                    onLimit();
                    added = i;
                    break;
                }
                else {
                    mNumbers.add(sum);
                    a = b;
                    b = sum;
                }
            }
        }
        addAvailable(added);
    }

    // Initialize variables that are used by a method in the constructor
    public void initialize() {
        mNumbers = new ArrayList<Long>();
        a = 0;
        b = 1;
        round = 50;
    }
}
